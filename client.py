import socketio
import socket

# Se connecter au serveur Flask via Socket.IO
sio = socketio.Client()
sio.connect('http://127.0.0.1:9002')

# Se connecter au serveur de messages via socket
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host, port = "127.0.0.1", 9002
client_socket.connect((host, port))

nom = input("Quelle est votre nom ? ")

print("\nBienvenue dans le chat!!\n")

while True:
    message = input(f"{nom} > ")

    # Envoyer le message au serveur de messages via socket
    client_socket.send(f"{nom} > {message}".encode("utf-8"))

    # Recevoir les messages du serveur
    donnees_recues = client_socket.recv(1024).decode("utf-8")
    print(donnees_recues)

    # Envoyer le message au serveur Flask via Socket.IO
    sio.emit('message', {'message': donnees_recues})
