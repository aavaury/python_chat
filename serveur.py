from flask import Flask, render_template
from flask_socketio import SocketIO, emit
from flask import send_file
from flask import request

app = Flask(__name__, template_folder='templates')
socketio = SocketIO(app)

@app.route('/')
def index():
    return render_template('index.html')

@socketio.on('message')
def handle_message(data):
    message = data.get('message', '')
    emit('message', {'message': message}, broadcast=True)


@app.route('/chat')
def chat():
    username = request.args.get('username')
    return render_template('chat.html', username=username)


if __name__ == '__main__':
    socketio.run(app, debug=True, port=9002)
    

